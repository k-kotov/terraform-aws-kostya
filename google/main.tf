resource "google_compute_network" "network" {
  name = "tf-test"
}

provider "google" {
  #credentials = "/home/boo/Downloads/scalr-development-ca0dcdb80414.json"
  project     = "development-156220"
  region      = "us-central1"
  zone        = "us-central1-c"
  }

resource "google_compute_instance" "default" {
  name         = "kkotov-test-gce123"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"
  project      = "development-156220"
  #zone         = data.google_compute_zones.available.names[0] 
  network_interface {
    # A default network is created for all GCP projects
    network       = "terraform-network"
    subnetwork    = "terraform-subnetwork"
    access_config {
    }
   }

  tags = ["foo", "bar"]
boot_disk {
 initialize_params {
    image = "gce-uefi-images/windows-1809-core"
   }
  }
 }
 
 data "google_client_config" "current" {}

data "google_container_engine_versions" "default" {
  #project = "scalr-labs"
  location = "us-central1-a"
}
