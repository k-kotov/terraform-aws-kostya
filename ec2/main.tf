provider "aws"  {
  region   = var.region
  
  }

resource "aws_instance" "Windows_2019_kostya" {
  #ami                         = data.aws_ami.windows.id
  #count = 40
  cpu_core_count = 1
  cpu_threads_per_core = 1
  ami                            =  var.win_ami
  instance_type               = var.instance_type_win
  subnet_id                   = var.subnet
  associate_public_ip_address = var.associate_public_ip

    tags = merge({ "Name" = format("k.kotov-test -> %s -> %s", substr ("🤔🤷", 0,1), data.aws_ami.windows.name) }, var.tags)
    timeouts {
    create = "10m"
    delete = "15m"
       }
  #tags = merge({ "Name" = "k.kotov"}, var.tags)
 }




data "aws_ami" "windows" {
  most_recent = true
  
  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base-2020.01.15*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
owners = ["801119661308"]
}
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}

resource "aws_ebs_volume" "ebs-volume_for_testing_terraform-AllowFullS3Access" {
  availability_zone = "us-east-1a"
        size = 30
  tags = merge({ "Name" = "k.kotov"}, var.tags)
    }

data "aws_iam_policy_document" "bucket_policy" {
  statement {
    sid       = "AllowFullS3Access"
    actions   = ["s3:ListAllMyBuckets"]
    resources = ["*"]
  }
}

# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "terraform_example2000"
  description = "Used in the terraform"
  vpc_id      = "vpc-596aa03e"
  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP access from the VPC
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#resource "aws_ecr_repository" "foo" {
 # name = "bar78"
#}

#resource "aws_ecr_repository_policy" "foopolicy" {
 # repository = "${aws_ecr_repository.foo.name}"

  #policy = <<EOF
#{
 #   "Version": "2008-10-17",
 #   "Statement": [
 #       {
 #           "Sid": "new policy",
 #          "Effect": "Allow",
 #           "Principal": "*",
 #           "Action": [
 #               "ecr:GetDownloadUrlForLayer",
 #               "ecr:BatchGetImage",
 #               "ecr:BatchCheckLayerAvailability",
 #               "ecr:PutImage",
 #               "ecr:InitiateLayerUpload",
 #               "ecr:UploadLayerPart",
 #               "ecr:CompleteLayerUpload",
 #               "ecr:DescribeRepositories",
 #               "ecr:GetRepositoryPolicy",
 #               "ecr:ListImages",
 #               "ecr:DeleteRepository",
 #               "ecr:BatchDeleteImage",
 #               "ecr:SetRepositoryPolicy",
 #               "ecr:DeleteRepositoryPolicy"
 #           ]
 #       }
  #  ]
#}
#EOF
#}


variable "letters" {
  description = "a list of letters"
  default = ["a", "b", "c"]
}

# Convert letters to upper-case as list
output "upper-case-list" {
  value = [for l in var.letters: upper(l)]
}

# Convert letters to upper-case as map
output "upper-case-map" {
  value = {for l in var.letters: l => upper(l)}
}
