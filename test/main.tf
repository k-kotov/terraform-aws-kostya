resource "google_sql_database_instance" "master" {
	count = 1
	name = var.name
 	database_version = "MYSQL_5_6"
 	master_instance_name = "kkotovtestqa"
	region = "us-central1"
	

       settings {
  	    tier = "db-f1-micro"

    	    ip_configuration {
     		ipv4_enabled = false
   #    		private_network = "${google_compute_network.default.self_link}"
	    }
  		
    	    activation_policy = "ALWAYS"
            disk_size = 0
            disk_type = "PD_HDD"
            backup_configuration {
                   enabled = false
            }

		}

            #replica_configuration {
            #}
       }
resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network6660122"
  auto_create_subnetworks = "true"
}

resource "google_compute_instance" "default" {
  name         = "kkotov-test-gce"
  machine_type = "n1-standard-1"
  zone         = "us-east1-b"
   network_interface {
    # A default network is created for all GCP projects
    network       = "terraform-network"
    access_config {
    }
   }

  tags = ["foo", "bar"]
boot_disk {
 initialize_params {
    image = "gce-uefi-images/windows-1803-core"
 }
}
}

data "google_container_engine_versions" "default" {
  project = "development-156220"
  location = "us-central1-a"
}







provider "google" {
		}
# Configure the master instance
resource "google_sql_database_instance" "pgsql" {
  name             = "${var.name}"
  database_version = "POSTGRES_9_6"
  region           = "${var.region}"
  settings {
    tier              = "${var.cloud_sql_postgres_tier}"
    activation_policy = "ALWAYS"
    disk_autoresize   = true
    disk_type         = "PD_SSD"
    backup_configuration {
      enabled    = true
      start_time = "${var.backup_start_time}"
    }
    ip_configuration {
      ipv4_enabled = true
      require_ssl  = true
    }
  }
}
resource "google_sql_user" "admin-user" {
  name     = "admin"
  instance = "${google_sql_database_instance.pgsql.name}"
  password = "${var.admin_password}"
}
resource "google_project_iam_member" "sql-client-iam" {
  role   = "roles/cloudsql.admin"
  member = "serviceAccount:${var.service_account_email}"
}
data "google_compute_network" "network" {
  name    = "${var.network}"
  project = "${var.network_project == "" ? var.project : var.network_project}"
}

data "google_compute_subnetwork" "network" {
  name    = "${var.subnetwork}"
  project = "${var.network_project == "" ? var.project : var.network_project}"
  region                = "${var.region}"
}

resource "google_compute_forwarding_rule" "default" {
  project               = "${var.project}"
  name                  = "${var.name}"
  region                = "${var.region}"
  network               = "${data.google_compute_network.network.self_link}"
  subnetwork            = "${data.google_compute_subnetwork.network.self_link}"
  load_balancing_scheme = "INTERNAL"
  backend_service       = "${google_compute_region_backend_service.default.self_link}"
  ip_address            = "10.0.0.0"
  ip_protocol           = "${var.ip_protocol}"
  ports                 = [80]
}

resource "google_compute_region_backend_service" "default" {
  project          = "${var.project}"
  name             = "${var.name}"
  region           = "${var.region}"
  protocol         = "${var.ip_protocol}"
  timeout_sec      = 10
  session_affinity = "${var.session_affinity}"
  #backend          = ["${var.backends}"]
  health_checks    = ["${element(compact(concat(google_compute_health_check.tcp.*.self_link,google_compute_health_check.http.*.self_link)), 0)}"]
}

resource "google_compute_health_check" "tcp" {
  count = "${var.http_health_check ? 0 : 1}"
  project = "${var.project}"
  name    = "${var.name}-hc"

  tcp_health_check {
    port = "${var.health_port}"
  }
}

resource "google_compute_health_check" "http" {
  count = "${var.http_health_check ? 1 : 0}"
  project = "${var.project}"
  name    = "${var.name}-hc"

  http_health_check {
    port = "${var.health_port}"
  }
}

resource "google_compute_firewall" "default-ilb-fw" {
  project = "${var.network_project == "" ? var.project : var.network_project}"
  name    = "${var.name}-ilb-fw"
  network = "name"

  allow {
    protocol = "${lower(var.ip_protocol)}"
    ports    = [80]
  }

  source_tags = ["allo7"]
  target_tags = ["allow9"]
}

resource "google_compute_firewall" "default-hc" {
  project = "${var.network_project == "" ? var.project : var.network_project}"
  name    = "${var.name}-hc"
  network = "name"

  allow {
    protocol = "tcp"
    ports    = ["${var.health_port}"]
  }

  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  target_tags   = ["allow111"]
}
